//
// Created by Piotr Rudnicki on 29/03/2024.
//

#include "MainWindow.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>

#include "../Services/Converters/ConverterFactory.h"
#include "../Services/Converters/Converter.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    setWindowTitle("Unit Converter");
    converterFactory = new ConverterFactory();
    auto availableConverters = converterFactory->getAvailableConverters();
    
    auto centralWidget = new QWidget(this);
    auto layout = new QVBoxLayout(centralWidget);

    auto label = new QLabel("Enter value to convert:", centralWidget);
    layout->addWidget(label);
    
    inputEdit = new QLineEdit(centralWidget);
    layout->addWidget(inputEdit);

    comboBox = new QComboBox(centralWidget);
    for (const auto& converter : availableConverters) {
        comboBox->addItem(QString::fromStdString(converter));
    }
    layout->addWidget(comboBox);

    convertButton = new QPushButton("Convert", centralWidget);
    connect(convertButton, &QPushButton::clicked, this, &MainWindow::convert);
    layout->addWidget(convertButton);

    resultEdit = new QLineEdit(centralWidget);
    resultEdit->setReadOnly(true);
    layout->addWidget(resultEdit);

    setCentralWidget(centralWidget);
}

void MainWindow::convert() {
    auto currentConverterName = comboBox->currentText().toStdString();
    auto converter = converterFactory->createConverter(currentConverterName);
    if(!converter) return;
    double input = inputEdit->text().toDouble();
    double result = converter->convert(input);
    resultEdit->setText(QString::number(result));
}