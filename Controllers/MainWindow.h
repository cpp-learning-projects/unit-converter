//
// Created by Piotr Rudnicki on 29/03/2024.
//

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include<QMainWindow>

class QLineEdit;
class QComboBox;
class QPushButton;
class ConverterFactory;

class MainWindow : public QMainWindow{
    Q_OBJECT
public:
    explicit MainWindow(QWidget* parent = nullptr);

private slots:
    void convert();
    
private:
    ConverterFactory* converterFactory;
    QLineEdit* inputEdit;
    QComboBox* comboBox;
    QPushButton* convertButton;
    QLineEdit* resultEdit;
};



#endif //MAINWINDOW_H
