//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef FAHRENHEITTOCELSIUSCONVERTER_H
#define FAHRENHEITTOCELSIUSCONVERTER_H
#include "Converter.h"


class FahrenheitToCelsiusConverter : public Converter{
public:
    double convert(double fahrenheit) override;
};



#endif //FAHRENHEITTOCELSIUSCONVERTER_H
