//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "MilesToKilometersConverter.h"

double MilesToKilometersConverter::convert(double miles) {
    return miles * 1.60934;
}