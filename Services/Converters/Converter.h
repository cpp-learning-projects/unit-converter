//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef CONVERTER_H
#define CONVERTER_H

class Converter
{
public:
    virtual ~Converter() = default;
    virtual double convert(double input) = 0;
};

#endif //CONVERTER_H
