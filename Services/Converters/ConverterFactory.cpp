//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "ConverterFactory.h"

#include "CelsiusToFahrenheitConverter.h"
#include "FahrenheitToCelsiusConverter.h"
#include "KilometersToMilesConverter.h"
#include "MilesToKilometersConverter.h"

ConverterFactory::ConverterFactory() {
    registerConverter("Celsius to Fahrenheit", []() { return std::make_unique<CelsiusToFahrenheitConverter>(); });
    registerConverter("Fahrenheit to Celsius", []() { return std::make_unique<FahrenheitToCelsiusConverter>(); });
    registerConverter("Kilometers to Miles", []() { return std::make_unique<KilometersToMilesConverter>(); });
    registerConverter("Miles to Kilometers", []() { return std::make_unique<MilesToKilometersConverter>(); });
}

std::unique_ptr<Converter> ConverterFactory::createConverter(const std::string& name) {
    if(converters.find(name) != converters.end())
    {
        return converters[name]();
    }
    return nullptr;
}

std::vector<std::string> ConverterFactory::getAvailableConverters() const {
    std::vector<std::string> names;
    for(const auto& pair : converters)
    {
        names.push_back(pair.first);
    }
    return names;
}

void ConverterFactory::registerConverter(const std::string& name, std::function<std::unique_ptr<Converter>()> creator) {
    converters[name] = creator;
}
