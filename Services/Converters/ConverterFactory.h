//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef CONVERTERFACTORY_H
#define CONVERTERFACTORY_H
#include <string>
#include <unordered_map>
#include <functional>

class Converter;

class ConverterFactory {
public:
    ConverterFactory();
    std::unique_ptr<Converter> createConverter(const std::string& name);
    std::vector<std::string> getAvailableConverters() const;

private:
    std::pmr::unordered_map<std::string, std::function<std::unique_ptr<Converter>()>> converters;
    void registerConverter(const std::string& name, std::function<std::unique_ptr<Converter>()> creator);
};



#endif //CONVERTERFACTORY_H
