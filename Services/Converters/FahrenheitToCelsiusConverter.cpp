//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "FahrenheitToCelsiusConverter.h"

double FahrenheitToCelsiusConverter::convert(double fahrenheit) {
    return (fahrenheit - 32.0) * 5.0 / 9.0;
}