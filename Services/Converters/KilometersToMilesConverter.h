//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef KILOMETERSTOMILESCONVERTER_H
#define KILOMETERSTOMILESCONVERTER_H
#include "Converter.h"


class KilometersToMilesConverter : public Converter{
public:
    double convert(double kilometers) override;
};



#endif //KILOMETERSTOMILESCONVERTER_H
