//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef MILESTOKILOMETERSCONVERTER_H
#define MILESTOKILOMETERSCONVERTER_H
#include "Converter.h"


class MilesToKilometersConverter : public Converter{
public:
    double convert(double miles) override;
};



#endif //MILESTOKILOMETERSCONVERTER_H
