//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "KilometersToMilesConverter.h"

double KilometersToMilesConverter::convert(double kilometers) {
    return kilometers * 0.621371;
}