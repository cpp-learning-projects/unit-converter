//
// Created by Piotr Rudnicki on 30/03/2024.
//

#include "CelsiusToFahrenheitConverter.h"

double CelsiusToFahrenheitConverter::convert(double input) {
    return input * 9.0 / 5.0 + 32.0;
}