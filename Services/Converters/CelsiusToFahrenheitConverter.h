//
// Created by Piotr Rudnicki on 30/03/2024.
//

#ifndef CELSIUSTOFAHRENHEITCONVERTER_H
#define CELSIUSTOFAHRENHEITCONVERTER_H
#include "Converter.h"


class CelsiusToFahrenheitConverter : public Converter{
public:
    double convert(double input) override;
};



#endif //CELSIUSTOFAHRENHEITCONVERTER_H
